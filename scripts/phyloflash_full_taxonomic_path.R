#!/usr/bin/env Rscript

### setting up environment ####
# Check if packages are installed
package.list <- c(
  "crayon",
  "optparse",
  "config",
  "tidyverse",
  "data.table"
)

# Function to check if packages are installed
is.installed <- function(pkg) {
  is.element(pkg, installed.packages()[, 1])
}

# If not all packages are available
if(any(!is.installed(package.list))) {
  cat("Not all required packages are available. They will now be installed.\n")
  
  # give the user the chance to abort manually
  Sys.sleep(20)
  
  # then install packages
  for(i in which(!is.installed(package.list))) {
    suppressMessages(install.packages(package.list[i], repos = "http://cran.us.r-project.org"))
  }
}

# Break the script if the package installation was unsuccessful
if(any(!is.installed(package.list))) {
  cat(
    paste0(
      "Unable to install all required packages.\nPlease install ",
      paste0(package.list[!is.installed(package.list)], collapse = ", "),
      " manually."
    )
  )
  break
}

# Load packages
cat("Loading libraries...")
silent <- suppressMessages(lapply(package.list, function(X) { require(X, character.only = TRUE) }))
rm(silent)
cat(" done\n")

# Some functions for message output
msg <- function(X){
  cat(crayon::white(paste0("[",format(Sys.time(), "%T"), "]")), X)
}

### Reading command line options ####

# define command line options
option_list <- list(
  make_option(
    c("-s", "--sam"),
    type = "character",
    default = NULL,
    help = "name (and path) of SAM file",
    metavar = "character"
  ),
  make_option(
    c("-o", "--output"),
    type = "character",
    default = NULL,
    help = "name (and path) of output file",
    metavar = "character"
  )
)

opt_parser <- OptionParser(option_list = option_list)
opt <- parse_args(opt_parser)

if (is.null(opt$sam) | is.null(opt$output)) {
  print_help(opt_parser)
  stop("All parameters are mandatory.\n", call. = FALSE)
}

### parse phyloflash SAM file ####

# define SAM flags to keep
sam_keep <- c(
  113,
  129,
  137,
  145,
  147,
  153,
  161,
  163,
  177,
  65,
  73,
  81,
  83,
  89,
  97,
  99
)

# read SAM file, filter to SAM flags of interest and keep only 1 hit per pair
sam_file <- data.frame(
  fread(
    opt$sam,
    h = F,
    sep = "\t",
    select = 1:3,
    fill = TRUE,
    quote = ""
  )
) %>% 
  filter(V2 %in% sam_keep) %>% 
  group_by(V1) %>%
  filter(row_number() == 1) %>%
  ungroup() %>% 
  separate(
    V3,
    into = c("accnos", "path"),
    sep = " ",
    extra = "merge"
  ) %>%
  as.data.frame()

# count taxonomic paths
out <- data.frame(
  table(sam_file$path)
)
colnames(out) <- c("path", "count")
msg(paste0("There are ", nrow(out), " unique taxonomic paths.\n"))

write.table(
  out,
  opt$output,
  row.names = F,
  quote = F,
  sep = "\t"
)