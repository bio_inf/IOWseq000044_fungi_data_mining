cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/phyloflash

# which flags are included in our phyloflash output?
cat ./*/*.bbmap.sam | cut -f2 | sort | uniq -c

# available SAM flags:
"""
101	no
113	yes
129	yes
133	no
137	yes
145	yes
147	yes
153	yes
161	yes
163	yes
165	no
177	yes
321	no
323	no
329	no
337	no
339	no
345	no
353	no
355	no
369	no
371	no
385	no
387	no
393	no
401	no
403	no
409	no
417	no
419	no
433	no
435	no
65	yes
69	no
73	yes
81	yes
83	yes
89	yes
97	yes
99	yes
"""

# process sam files one after the other
# write executable R script that will:
#   filter second column according to flag
#   keep only 1 hit per query (counting fragments instead of reads)
#   count number of occurrences for each taxonomic path
conda activate r-4.2.2
ls -1d *_pf_out | cut -d'_' -f1 | while read line
do
  echo $line
  ./phyloflash_full_taxonomic_path.R -s ${line}_pf_out/${line}.bbmap.sam -o ${line}_pf_out/${line}_full_taxonomy.txt
done

