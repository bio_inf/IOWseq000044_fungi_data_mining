conda activate bbmap-39.01
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
cat minlength100.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=60 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=60 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=100 trimpolyg=10 entropy=0.4 entropywindow=30 entropyk=5 threads=60 fastawrap=300 > ${line}.log3 2>&1
done 
cat minlength75.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=60 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=60 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=75 trimpolyg=10 entropy=0.4 entropywindow=30 entropyk=5 threads=60 fastawrap=300 > ${line}.log3 2>&1
done 
cat minlength60.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=60 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=60 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=60 trimpolyg=10 entropy=0.4 entropywindow=30 entropyk=5 threads=60 fastawrap=300 > ${line}.log3 2>&1
done 

cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"
conda deactivate
conda activate coverm-0.6.1
cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/minlength*.txt | while read line
do
bwa mem -v 1 -t 38 ../refgenome/ref_index_all_fungi.fna ${QT}/${line}_1_qt.fastq.gz ${QT}/${line}_2_qt.fastq.gz > ${line}.sam
samtools sort -T tmp_${line}_samtools -@ 38 -O BAM -o ${line}_.bam ${line}_.sam
rm ${line}_.sam
done

conda deactivate
conda activate coverm-0.6.1
coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 70 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output_all_metagenomes70.txt -t 30

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 95 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output_all_metagenomes95.txt -t 30


# breadth of coverage automatical for all metagenomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
conda deactivate
conda activate msamtools-1.1.2

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j20 'msamtools filter -b -p 70 -z 50 {}_entropy.bam > {}_entropy_filt1.bam'

# SRR3727511_filt1.bam
conda deactivate
conda activate coverm-0.6.1

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j30 'samtools view -f 2 -F 2304 -b {}_entropy_filt1.bam > {}_entropy_filt2.bam'

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j30 'samtools depth {}_entropy_filt2.bam > samtools_depth_entropy_{}.txt'
# -b -> write output in bam-format; -p -> threshold for %-identity; -z -> threshold for read-aligned-%

# summarize entries in samtools_depth_test.txt for every reference-genome
# 3 tables: 1. containing the samtools output -> how many positions/basepairs map the reference -> divided by regions of genome/chromosome; 2. containing the regions of genome/chromosome assigned to the reference-genome; 3. containing the genome size of every reference-genome
# 1st table
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
cut -f1 samtools_depth_entropy_${line}.txt | sort | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' > depth_samtools_output_entropy_${line}.txt
done


