
# access to the server -> bio-40 or bio-48
ssh bio-40
# creating directorys for the data of the thesis project
cd /bio/Analysis_data
mkdir IOWseq000044_fungi_data_mining
# creating 3 directorys for sorting of the generated data -> Validated_data and Intermediate_results will be deleted after the project
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining
mkdir Validated_data
mkdir Intermediate_results
mkdir Final_results
# download of the ENA data to Validated_data
cd Validated_data
screen -S name # screen -> even if the VPN is disconnected, the data will be downloaded
wget http:// # + link in fastq_ftp -> download starts
screen -ls
# Strg + A + D -> leave the download, without stoping it

# take a look at txt files
less filename # just look at the file, without modifying -> leave the display with Q
vi filename # look at the file and modifications -> I for editing -> Esc and then :wq for saving the modifications

# check sequence quality
conda activate qc-env # for environment (e.g. qc-env), see Gitea -> bio_inf/server_setup -> Wiki -> Programs and conda environments -> fastqc
fastqc -h
mkdir fastqc_out
fastqc ERR4352609_1.fastq.gz -o fastqc_out # fastq dateiname -o output_directory
fastqc ERR4352609_2.fastq.gz -o fastqc_out
fastqc ERR4352608_1.fastq.gz -o fastqc_out
fastqc ERR4352608_2.fastq.gz -o fastqc_out
fastqc *_[12].fastq.gz -o fastqc_out -t 24

rm ERR4352609_1.fastq.gz ERR4352609_2.fastq.gz ERR4352608_1.fastq.gz ERR4352608_2.fastq.gz
rm *_[12].fastq.gz

# copy html file from the server to the local directory ./ -> to the directory, where I am (/c/Users/maris/Documents/IOWseq000044_fungi_data_mining/fastqc_data)
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/fastqc_out/*html ./
# check the html for a few sequences of the study -> focus on "Per base sequence quality" and "Adapter Content"

# important values, that need to be checked sometimes!!!
htop # CPU% -> max. 4000 % | MEM% -> max. 100 % 		--> MEM% shouldn't reach 100 %, because then the server crashes and must be newstarted
#´ leave htop with Q
df -h # disk space overview
du -h -d1 # disk space for a certain directory

# Download for many datasets
# cut -f40 Baltic_metaG_filtered.txt | sed '1d' | tr ';' '\n' | sed 's/^/http:\/\//' > download.links
# conda env create -f /bio/Common_repositories/server_setup/conda_envs/aria2.yaml -n aria2-1.34.0

# manually prepare download links
# run on local git bash -> directory fastqc_data
dos2unix selection_data_download.txt

# copy with scp to bio servers -> lokal -> /c/Users/maris/Documents/IOWseq000044_fungi_data_mining/fastqc_data
scp ./selection_data_download.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
# next steps in the directory Validated_data on the server
screen -S datadownload
conda activate aria2-1.34.0
aria2c -i selection_data_download.txt -c --dir /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=20 &>> download.log
# -i -> input file; -c -> Continue downloading a partially downloaded file; --dir -> directory for saving the downloaded file; --max-tries -> set number of tries, when e.g. the download failes; --retry-wait -> set the seconds to wait between retries; --max-connection-per-server -> maximum number of connections to one server for each download; max-concurrent-downloads -> maximum number of parallel downloads
# check, how far the download processed
ls -ltrh
tail download.log
less download.log
tail -50 download.log
tail -f download.log #follow the download.log -> exit with strg + c


#missing data
# manually prepare download links
# run on local git bash -> directory fastqc_data
dos2unix selection_data_download_missing.txt

# copy with scp to bio servers -> lokal -> /c/Users/maris/Documents/IOWseq000044_fungi_data_mining/fastqc_data
scp ./selection_data_download_missing.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
# next steps in the directory Validated_data on the server
screen -S datadownload
conda activate aria2-1.34.0
aria2c -i selection_data_download_missing.txt -c --dir /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=20 &>> download.log

# source of the general BBDuk commands on Gitea: bio_inf/workflow_templates/metaG_Illumina_PE/rules/trimming.smk
# BBDuk commands -> directory /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
# rule remove_phix:
conda deactivate
conda activate bbmap-39.01
bbduk.sh in=ERR4352608_1.fastq.gz in2=ERR4352608_2.fastq.gz out=ERR4352608_1_phix.fastq.gz out2=ERR4352608_2_phix.fastq.gz ref=phix k=28 stats=ERR4352608.stats1 threads=24 fastawrap=300 > ERR4352608.log1 2>&1
# in + in2 -> for paired-reads in 2 files; out -> output; k -> kmer size; PhiX -> a common Illumina spikein; stats -> produce a report of which contaminant sequences were seen, and how many reads had them; threads={threads} -> parallelization, e.g. use all threads; fastawrap -> just relevant for fasta files
# threads adaptation depending on htop (bio-40 max. 64, bio-48 max. 40 -> not more than the half)

# rule remove_adapter_right:
bbduk.sh in=ERR4352608_1_phix.fastq.gz in2=ERR4352608_2_phix.fastq.gz out=ERR4352608_1_adapter.fastq.gz out2=ERR4352608_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=ERR4352608.stats3 threads=24 tpe fastawrap=300 > ERR4352608.log2 2>&1
#  ktrim=r -> right-trimming; mink -> shorter kmer at the end of the read; hdist -> number of mismatches; tpe -> specifies to trim both reads to the same length (tbo -> specifies to also trim adapters based on pair overlap detection using BBMerge -> overlap is not relevant for us)

# rule quality_trimming:
bbduk.sh in=ERR4352608_1_adapter.fastq.gz in2=ERR4352608_2_adapter.fastq.gz out=trimming/ERR4352608_1_qt.fastq.gz out2=trimming/ERR4352608_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=100 trimpolyg=10 threads=24 fastawrap=300 > ERR4352608.log3 2>&1
# qtrim -> average quality of 4 base; sliding window, window size 4, average quality threshold 15; minlength -> minimal length of the read, before it will be removed -> adapt for every group of samples, that are generated equally (same instrument model in one study), 50-75 % of the generated read-length; trimpolyg -> homopolymers consisting only of G -> just occure if very shot sequences are sequenced and non-sense-sequenzen are synthesized

#Schleife BBduk
# source of the general BBDuk commands on Gitea: bio_inf/workflow_templates/metaG_Illumina_PE/rules/trimming.smk
# BBDuk commands -> directory /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
# rule remove_phix:
conda deactivate
conda activate bbmap-39.01

# save run accession for samples to be processed (convert to line endings linux)
cat > run_accessions_PRJEB38290.txt
ERR4352720
ERR4352719
ERR4352596
ERR4352597
^C #strg + c

# check that loop works
cat run_accessions_PRJEB38290.txt | while read line
do
  echo ${line}
done 


cat run_accessions_PRJEB38290.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=60 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=60 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=100 trimpolyg=10 threads=60 fastawrap=300 > ${line}.log3 2>&1
done 
 
 #2 done
cat > run_accessions_PRJEB22997.txt
ERR2206761
ERR2206760
ERR2206772
ERR2206769
ERR2206762
ERR2206774
^C

cat run_accessions_PRJEB22997.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=24 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=24 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=75 trimpolyg=10 threads=24 fastawrap=300 > ${line}.log3 2>&1
done 

#3 done
cat > run_accessions_PRJNA273799.txt
SRR2053292
SRR2053302
SRR2053291
SRR2032810
SRR2053277
SRR2053287
^C

cat run_accessions_PRJNA273799.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=24 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=24 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=60 trimpolyg=10 threads=24 fastawrap=300 > ${line}.log3 2>&1
done 

#4 done
cat > run_accessions_PRJNA322246.txt
SRR3727526
SRR3727507
SRR3727511
SRR3727516
^C

cat run_accessions_PRJNA322246.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=24 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=24 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=50 trimpolyg=10 threads=24 fastawrap=300 > ${line}.log3 2>&1
done 

#fastqc for the data after quality trimming
# check sequence quality
conda activate qc-env # for environment (e.g. qc-env), see Gitea -> bio_inf/server_setup -> Wiki -> Programs and conda environments -> fastqc
fastqc -h
mkdir fastqc_out_qt
fastqc ERR4352609_1_qt.fastq.gz -o fastqc_out_qt # fastq dataname -o output_directory
fastqc ERR4352609_2_qt.fastq.gz -o fastqc_out_qt
fastqc ERR4352608_1_qt.fastq.gz -o fastqc_out_qt
fastqc ERR4352608_2_qt.fastq.gz -o fastqc_out_qt
fastqc * -o fastqc_out_qt -t 30
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/fastqc_out_qt/*html ./
# move a file
mv ERR4352608_2_fastqc.html /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/fastqc_out
mv /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/ERR4352608_2_fastqc.html ./
# in fastqc_out folder:
mv ../*html ./
mv ../*zip ./

# push files to Gitea -> ~/Documents/IOWseq000044_fungi_data_mining/repository/IOWseq000044_fungi_data_mining
git status # shows all differences between the lokal repository and the repository on Gitea
git add . # if you want to add all (.)
git commit -m "message"
git push # now the push is completed
git log # history of all pushes

# download database for taxonomy
wget https://ftp.ncbi.nlm.nih.gov/pub/taxonomy/taxdump.tar.gz
tar -xzvf taxdump.tar.gz

dos2unix results_assembly_tsv_20230413-1221.txt
scp results_assembly_tsv_20230413-1221.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/taxonkit_output/

conda env list
conda activate taxonkit-0.14.0

head -1 results_assembly_tsv_20230413-1221.txt | tr '\t' '\n' | grep -n "tax_id" # 18:tax_id
cut -f18 results_assembly_tsv_20230413-1221.txt
cut -f18 results_assembly_tsv_20230413-1221.txt | sed '1d' # sed um die erste Zeile der Spalte zu entfernen (tax_id)

# lineage

cut -f18 results_assembly_tsv_20230413-1221.txt | sed '1d' | taxonkit lineage --data-dir ../taxdump -R > taxonkit_output_chytrids.txt
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/taxonkit_output/taxonkit_output_chytrids.txt ./

# automatical download of the associated studies with the study_accessions
cut -f14 results_assembly_tsv_20230413-1221.txt | sed '1d' | sort | uniq | while read line
do
  wget https://www.ebi.ac.uk/ena/xref/rest/tsv/search?accession=${line} -O references_refgenome.txt
  sed '1d' references_refgenome.txt >> Chytridiomycota_xref_results_study.txt
  rm references_refgenome.txt
done

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/taxonkit_output/Chytridiomycota_xref_results_study.txt ./

# include Helgoland-study with Cerataulina
# copy with scp to bio servers -> lokal -> /c/Users/maris/Documents/IOWseq000044_fungi_data_mining/ENA
scp ./cerataulina_data_selection.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
# next steps in the directory Validated_data on the server
screen -S datadownload
conda activate aria2-1.34.0
aria2c -i cerataulina_data_selection.txt -c --dir /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=20 &>> download.log

# fastqc /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
conda deactivate
conda activate qc-env
fastqc ERR9769* -o fastqc_out -t 24

# /c/Users/maris/Documents/IOWseq000044_fungi_data_mining/fastqc_data
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/fastqc_out/ERR9769*html ./

#BBDuk /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
screen -S trimming
conda deactivate
conda activate bbmap-39.01
cat > run_accessions_PRJEB52999.txt
ERR9769377
ERR9769376
ERR9769378
ERR9769375
^C
screen -r trimming
cat run_accessions_PRJEB52999.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=24 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=24 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=100 trimpolyg=10 threads=24 fastawrap=300 > ${line}.log3 2>&1
done 

# 2. fastqc /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
fastqc ERR9769* -o fastqc_out_qt -t 48
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/fastqc_out_qt/*html ./

# delete all initial metagenome sequences for saving storage space on the server
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
ls -ltrh 
ls *gz
rm *gz

# refgenome Download
dos2unix download_links_refgenome.txt
scp ./download_links_refgenome.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome

cat download_links_refgenome.txt | parallel -j8 wget
gunzip GCA*

# build mapping index
# concatenate reference genomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome
cat fastaseq/*.fna > ref_index.fna
conda activate coverm-0.6.1
bwa index ref_index.fna

cd ..
mkdir mapping
cd mapping

# mapping for a single metagenome
screen -r 
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"
bwa mem -v 1 -t 32 ../refgenome/ref_index.fna ${QT}/ERR4352608_1_qt.fastq.gz ${QT}/ERR4352608_2_qt.fastq.gz > ERR4352608.sam
samtools sort -T tmp_ERR4352608_samtools -@ 32 -O BAM -o ERR4352608.bam ERR4352608.sam
rm ERR4352608.sam

# automatical mapping
screen -r 
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
bwa mem -v 1 -t 32 ../refgenome/ref_index.fna ${QT}/${line}_1_qt.fastq.gz ${QT}/${line}_2_qt.fastq.gz > ${line}.sam
samtools sort -T tmp_${line}_samtools -@ 32 -O BAM -o ${line}.bam ${line}.sam
rm ${line}.sam
done

# mapping for one metagenome, that was previously not included
screen -r 
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"
bwa mem -v 1 -t 32 ../refgenome/ref_index.fna ${QT}/ERR4352609_1_qt.fastq.gz ${QT}/ERR4352609_2_qt.fastq.gz > ERR4352609.sam
samtools sort -T tmp_ERR4352609_samtools -@ 32 -O BAM -o ERR4352609.bam ERR4352609.sam
rm ERR4352609.sam

#samtools view -q20 -F2304 -m50 -b file.bam

# CoverM 
conda activate coverm-0.6.1

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq/*.fna --min-read-percent-identity-pair 95 --min-read-aligned-percent-pair 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output1.txt -t 48

# try: --min-read-percent-identity and --min-read-aligned-percent; --min-read-percent-identity-pair 98 ;--min-read-aligned-percent-pair 70
# -m relative abundance (is default, had to be changed if you conduct filtering steps for the reads previously)
coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq/*.fna --min-read-percent-identity 95 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output2.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq/*.fna --min-read-percent-identity 80 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output3.txt -t 24

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq/*.fna --min-read-percent-identity 50 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output4.txt -t 24

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq/*.fna --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output5.txt -t 24

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq/*.fna --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_output6.txt -t 24

# run short read classification (kaiju) against all domains of life -> to find out what is the % fungi in the sample
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
ls -1 *_1_qt.fastq.gz | tr '\n' ','

screen -r 
R1=$(ls -1 *_1_qt.fastq.gz | tr '\n' ',' | sed 's/,$//')
R2=$(ls -1 *_2_qt.fastq.gz | tr '\n' ',' | sed 's/,$//')
OUT=$(ls -1 *_1_qt.fastq.gz | xargs -n1 basename | sed 's/_1_qt.fastq.gz//' | sed 's/$/_kaiju.txt/' | tr '\n' ',' | sed 's/,$//')
conda activate kaiju-1.9.2

kaiju-multi -z 38 -t /bio/Databases/Cross_domain/June2022/DB_taxonomy/nodes.dmp -f /bio/Databases/Cross_domain/June2022/kaiju/proteins.fmi -i ${R1} -j ${R2} -o ${OUT} -e 5 -E 0.1


cut -f3 ERR2206760_kaiju.txt | taxonkit lineage --data-dir /bio/Databases/Cross_domain/June2022/DB_taxonomy > ERR2206760_output.txt

cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
conda activate taxonkit-0.14.0

ls -1 *_1_qt.fastq.gz | cut -d'_' -f1 | while read line
do
cut -f3 ${line}_kaiju.txt | taxonkit lineage --data-dir /bio/Databases/Cross_domain/June2022/DB_taxonomy > ${line}_output.txt
done

# taxonkit summary
conda activate r-4.2.2
# /bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/summarize_kaiju.R -i *_kaiju.txt -o kaiju_summary.txt -t 38

# R script requires list of input files in quotations:
IN=$(ls -1 *_output.txt | tr '\n' ' ')
/bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/summarize_kaiju.R -i "$IN" -o kaiju_summary.txt -t 90

# copy to local laptop
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/kaiju_summary.txt ./


# Download of all 113 Chytrid-genomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/
mkdir fastaseq_all_chytrids

dos2unix download_links_refgenome_all.txt
scp ./download_links_refgenome_all.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids

cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids
cat download_links_refgenome_all.txt | parallel -j48 wget
gunzip GCA*

ls -1 *.fna | wc -l 
ls -1 *.fna > bin_list_fastani_chytrids.txt
mkdir fastani_out

# ANI 113 chytrid genomes
conda activate drep-3.2.2
fastANI --ql bin_list_fastani_chytrids.txt --rl bin_list_fastani_chytrids.txt -o fastani_out/fastani_comparison_chytrids.txt -t 38
# running again on phy-4 (slightly longer fraglength and without af filter
fastANI --ql bin_list_fastani_chytrids.txt --rl bin_list_fastani_chytrids.txt --fragLen 5000 --minFraction 0 -t 70 -o fastani_out/fastani_comparison_chytrids.txt

# Download Refseq-genomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/
mkdir fastaseq_refseq

dos2unix download_links_refseq.txt
scp ./download_links_refseq.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq

cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq
cat download_links_refseq.txt | parallel -j20 wget
gunzip GCF*

ls -1 ../fastaseq_refseq/*.fna | wc -l 
ls -1 ../fastaseq_refseq/*.fna > bin_list_fastani_refseq.txt

# ANI 113 chytrid genomes vs. 491 non-chytrid genomes
screen -S fastani
conda activate drep-3.2.2
fastANI --ql bin_list_fastani_chytrids.txt --rl bin_list_fastani_refseq.txt -o fastani_out/fastani_comparison_nonchytrids.txt -t 24
# unfortunately, the job on bio-40 crashed just short of finishing
# running again on phy-4 (slightly longer fraglength and without af filter
fastANI --ql bin_list_fastani_chytrids.txt --rl bin_list_fastani_refseq.txt --fragLen 5000 --minFraction 0 -t 70 -o fastani_out/fastani_comparison_nonchytrids.txt

# copy ANI-files to local laptop
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/fastani_out/fastani_comparison_chytrids.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/fastani_out/fastani_comparison_nonchytrids.txt ./

# since fastANI is taking too long for the required comparisons (even on the PHY servers), try mash distance (https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7711752/)
conda activate drep-3.2.2
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids
mkdir mash_out
mash sketch -p 60 -k 21 -s 100000 -o mash_out/chytrid -l bin_list_fastani_chytrids.txt
mash sketch -p 60 -k 21 -s 100000 -o mash_out/non_chytrid -l bin_list_fastani_refseq.txt
mash dist -p 60 mash_out/chytrid.msh mash_out/chytrid.msh > mash_out/mash_comparison_chytrids.txt
mash dist -p 60 mash_out/chytrid.msh mash_out/non_chytrid.msh > mash_out/mash_comparison_nonchytrids.txt

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/mash_out/*.txt ./

# calculate the Mb for a sequence
ls -1 *.fna | while read line
do 
  grep -v '^>' ${line} | perl -nle 'print length' | paste -sd+ | bc
done

#18S mapping with phyloflash
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash
conda activate phyloflash-3.4

ls -1 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/*.gz | cut -d'/' -f7 | cut -d'_' -f1 | while read line
do 
phyloFlash.pl -lib ${line} -read1 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/${line}_1_qt.fastq.gz -read2 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/${line}_2_qt.fastq.gz -dbhome /bio/Databases/SILVA/v138.1/phyloflash/138.1 -CPUs 32 -emirge > ${line}_pf.log 2>&1
done

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash/*html ./

# include promising studies from Christiane ERR3761427 and SRR10996962
# copy with scp to bio servers -> lokal -> /c/Users/maris/Documents/IOWseq000044_fungi_data_mining/ENA
scp ./promising_data_selection.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
# next steps in the directory Validated_data on the server
screen -S datadownload
conda activate aria2-1.34.0
aria2c -i promising_data_selection.txt -c --dir /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data --max-tries=20 --retry-wait=5 --max-connection-per-server=1 --max-concurrent-downloads=20 &>> download.log

# fastqc /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
conda deactivate
conda activate qc-env
fastqc *_[12].fastq.gz -o fastqc_out -t 30

# /c/Users/maris/Documents/IOWseq000044_fungi_data_mining/fastqc_data
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/fastqc_out/ERR3761427*html ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/fastqc_out/SRR10996962*html ./

#BBDuk /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data
cat > run_accessions_PRJEB34883_PRJNA603899.txt
ERR3761427
SRR10996962
^C
screen -r trimming
conda deactivate
conda activate bbmap-39.01

cat run_accessions_PRJEB34883_PRJNA603899.txt | while read line
do
  bbduk.sh in=${line}_1.fastq.gz in2=${line}_2.fastq.gz out=${line}_1_phix.fastq.gz out2=${line}_2_phix.fastq.gz ref=phix k=28 stats=${line}.stats1 threads=48 fastawrap=300 > ${line}.log1 2>&1
  
  bbduk.sh in=${line}_1_phix.fastq.gz in2=${line}_2_phix.fastq.gz out=${line}_1_adapter.fastq.gz out2=${line}_2_adapter.fastq.gz ref=adapters k=23 mink=11 ktrim=r hdist=1 stats=${line}.stats3 threads=48 tpe fastawrap=300 > ${line}.log2 2>&1
  
  bbduk.sh in=${line}_1_adapter.fastq.gz in2=${line}_2_adapter.fastq.gz out=trimming/${line}_1_qt.fastq.gz out2=trimming/${line}_2_qt.fastq.gz qtrim=w,4 trimq=15 minlength=85 trimpolyg=10 threads=48 fastawrap=300 > ${line}.log3 2>&1
done 

# 2. fastqc /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
conda deactivate
conda activate qc-env
fastqc ERR3761427* -o fastqc_out_qt -t 48
fastqc SRR10996962* -o fastqc_out_qt -t 48
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/fastqc_out_qt/ERR3761427*html ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/fastqc_out_qt/SRR10996962*html ./

# run short read classification (kaiju) against all domains of life -> to find out what is the % fungi in the sample for the new studies
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming

screen -r 
conda activate kaiju-1.9.2

kaiju-multi -z 20 -t /bio/Databases/Cross_domain/June2022/DB_taxonomy/nodes.dmp -f /bio/Databases/Cross_domain/June2022/kaiju/proteins.fmi -i ERR3761427_1_qt.fastq.gz,SRR10996962_1_qt.fastq.gz -j ERR3761427_2_qt.fastq.gz,SRR10996962_2_qt.fastq.gz -o ERR3761427_kaiju.txt,SRR10996962_kaiju.txt -e 5 -E 0.1

cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
conda activate taxonkit-0.14.0
cut -f3 ERR3761427_kaiju.txt | taxonkit lineage --data-dir /bio/Databases/Cross_domain/June2022/DB_taxonomy > ERR3761427_output.txt
cut -f3 SRR10996962_kaiju.txt | taxonkit lineage --data-dir /bio/Databases/Cross_domain/June2022/DB_taxonomy > SRR10996962_output.txt

# taxonkit summary
conda activate r-4.2.2
IN=$(ls -1 *_output.txt | tr '\n' ' ')
/bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/summarize_kaiju.R -i "$IN" -o kaiju_summary_all_fungi.txt -t 20

# copy to local laptop
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/kaiju_summary_all_fungi.txt ./

#18S mapping with phyloflash
screen -r 
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash
conda activate phyloflash-3.4

phyloFlash.pl -lib ERR3761427 -read1 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/ERR3761427_1_qt.fastq.gz -read2 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/ERR3761427_2_qt.fastq.gz -dbhome /bio/Databases/SILVA/v138.1/phyloflash/138.1 -CPUs 20 -emirge > ERR3761427_pf.log 2>&1

phyloFlash.pl -lib SRR10996962 -read1 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/SRR10996962_1_qt.fastq.gz -read2 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/SRR10996962_2_qt.fastq.gz -dbhome /bio/Databases/SILVA/v138.1/phyloflash/138.1 -CPUs 20 -emirge > SRR10996962_pf.log 2>&1

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash/*html ./

# analysis of phyloflash output
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash
grep -i "chytrid" *.phyloFlash.NTUfull_abundance.csv
grep -i "chytrid" *.all.vsearch.csv

#copy to local laptop
cd Documents/IOWseq000044_fungi_data_mining/reference_genome/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash/*.phyloFlash.NTUfull_abundance.csv ./

# MAPPING WITH 113 CHYTRIDGENOMES AND 491 REFSEQ-GENOMES
# build mapping index
# concatenate reference genomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome
cat fastaseq_all_chytrids/*.fna fastaseq_refseq/*.fna > ref_index_all_fungi.fna

conda activate coverm-0.6.1
bwa index ref_index_all_fungi.fna

cd ..
cd mapping

# add ERR4352608 and ERR4352609 to run_accessions_PRJEB38290.txt
vi run_accessions_PRJEB38290.txt

# automatical mapping
screen -r 
cd mapping
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"
conda activate coverm-0.6.1

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
bwa mem -v 1 -t 24 ../refgenome/ref_index_all_fungi.fna ${QT}/${line}_1_qt.fastq.gz ${QT}/${line}_2_qt.fastq.gz > ${line}.sam
samtools sort -T tmp_${line}_samtools -@ 24 -O BAM -o ${line}.bam ${line}.sam
rm ${line}.sam
done

# CoverM 
conda activate coverm-0.6.1

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-read-percent-identity-pair 95 --min-read-aligned-percent-pair 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output1.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-read-percent-identity-pair 80 --min-read-aligned-percent-pair 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output2.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-aligned-percent-pair 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output3.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity-pair 80 --min-read-aligned-percent-pair 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output4.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity-pair 95 --min-read-aligned-percent-pair 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output5.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 95 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output6.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 95 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -m count -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output7.txt -t 32

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 70 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output8.txt -t 50

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 70 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -m count -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output9.txt -t 50

ls mapping_all*.txt
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all*.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output6.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output7.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output8.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_all_fungi_output9.txt ./

# analysis in R -> genome with just very few total query fragments -> fastani for that genome again, but reverse
screen -S fastani
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids
conda activate drep-3.2.2
fastANI --ql bin_list_fastani_refseq.txt -r GCA_023347045.1_ASM2334704v1_genomic.fna -o fastani_out/fastani_comparison_nonchytrids_outlier.txt -t 50

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/fastani_out/fastani_comparison_nonchytrids_outlier.txt ./
# futher analysis -> see R

# breadth of coverage using msamtools -> test SRR2053292 
screen -r
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
conda deactivate
conda activate msamtools-1.1.2

msamtools filter -b -p 70 -z 50 SRR2053292.bam > SRR2053292_filt1.bam

conda deactivate
conda activate coverm-0.6.1

samtools view -f 2 -F 2304 -b SRR2053292_filt1.bam > SRR2053292_filt2.bam

samtools depth -a SRR2053292_filt2.bam > samtools_depth_test.txt
samtools depth SRR2053292_filt2.bam > samtools_depth_test.txt


less samtools_depth_test.txt
cut -f1 samtools_depth_test.txt | sort | uniq -c

cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids
grep '^>' GCA_002104895.1_Anaeromyces_sp._S4_v1.0_genomic.fna

# summarize entries in samtools_depth_test.txt for every reference-genome
# 3 tables: 1. containing the samtools output -> how many positions/basepairs map the reference -> divided by regions of genome/chromosome; 2. containing the regions of genome/chromosome assigned to the reference-genome; 3. containing the genome size of every reference-genome
# 1st table
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
cut -f1 samtools_depth_test.txt | sort | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' > depth_samtools_output.txt

# 2nd table
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids
ls -1 *.fna | while read line
do
  GI=$(echo ${line} | cut -d'_' -f1,2)
  grep '^>' ${line} | sed 's/ .*$//' | sed "s/^>/${GI}\t/"
done > /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth_accession_numbers.txt
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq
ls -1 *.fna | while read line
do
  GI=$(echo ${line} | cut -d'_' -f1,2)
  grep '^>' ${line} | sed 's/ .*$//' | sed "s/^>/${GI}\t/"
done >> /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth_accession_numbers.txt

# 3rd table
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids
ls -1 *.fna | while read line
do 
  grep -v '^>' ${line} | perl -nle 'print length' | paste -sd+ | bc | paste <(echo ${line} | cut -d'_' -f1,2) - 
done > /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth_genome_size.txt
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq
ls -1 *.fna | while read line
do 
  grep -v '^>' ${line} | perl -nle 'print length' | paste -sd+ | bc | paste <(echo ${line} | cut -d'_' -f1,2) - 
done >> /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth_genome_size.txt

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth*.txt ./






# automatical for all metagenomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
conda deactivate
conda activate msamtools-1.1.2

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j12 'msamtools filter -b -p 70 -z 50 {}.bam > {}_filt1.bam'


conda deactivate
conda activate coverm-0.6.1

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j12 'samtools view -f 2 -F 2304 -b {}_filt1.bam > {}_filt2.bam'

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j12 'samtools depth {}_filt2.bam > samtools_depth_{}.txt'
# -b -> write output in bam-format; -p -> threshold for %-identity; -z -> threshold for read-aligned-%

# summarize entries in samtools_depth_test.txt for every reference-genome
# 3 tables: 1. containing the samtools output -> how many positions/basepairs map the reference -> divided by regions of genome/chromosome; 2. containing the regions of genome/chromosome assigned to the reference-genome; 3. containing the genome size of every reference-genome
# 1st table
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
cut -f1 samtools_depth_${line}.txt | sort | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' > depth_samtools_output_${line}.txt
done

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth_samtools_output*.txt ./


# BBDuk entropy to remove  repetitive sequences in one metagenome using different entropy-settings
screen -r
conda deactivate
conda activate bbmap-39.01
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy1.fastq.gz out2=SRR10996962_2_entropy1.fastq.gz outm=SRR10996962_1_discard1.fastq.gz outm2=SRR10996962_2_discard1.fastq.gz entropy=0.01 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy2.fastq.gz out2=SRR10996962_2_entropy2.fastq.gz outm=SRR10996962_1_discard2.fastq.gz outm2=SRR10996962_2_discard2.fastq.gz entropy=0.1 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy3.fastq.gz out2=SRR10996962_2_entropy3.fastq.gz outm=SRR10996962_1_discard3.fastq.gz outm2=SRR10996962_2_discard3.fastq.gz entropy=0.2 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy4.fastq.gz out2=SRR10996962_2_entropy4.fastq.gz outm=SRR10996962_1_discard4.fastq.gz outm2=SRR10996962_2_discard4.fastq.gz entropy=0.3 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy5.fastq.gz out2=SRR10996962_2_entropy5.fastq.gz outm=SRR10996962_1_discard5.fastq.gz outm2=SRR10996962_2_discard5.fastq.gz entropy=0.4 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy6.fastq.gz out2=SRR10996962_2_entropy6.fastq.gz outm=SRR10996962_1_discard6.fastq.gz outm2=SRR10996962_2_discard6.fastq.gz entropy=0.5 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy7.fastq.gz out2=SRR10996962_2_entropy7.fastq.gz outm=SRR10996962_1_discard7.fastq.gz outm2=SRR10996962_2_discard7.fastq.gz entropy=0.6 entropywindow=30 entropyk=5 threads=30

bbduk.sh in=SRR10996962_1_qt.fastq.gz in2=SRR10996962_2_qt.fastq.gz out=SRR10996962_1_entropy8.fastq.gz out2=SRR10996962_2_entropy8.fastq.gz outm=SRR10996962_1_discard8.fastq.gz outm2=SRR10996962_2_discard8.fastq.gz entropy=0.7 entropywindow=30 entropyk=5 threads=30

# fastqc to check which effect the different entropy settings have 
conda deactivate
conda activate qc-env
fastqc *entropy*.fastq.gz -o fastqc_out_entropy -t 30
fastqc *entropy7.fastq.gz -o fastqc_out_entropy -t 30
fastqc *entropy8.fastq.gz -o fastqc_out_entropy -t 30

fastqc *discard*.fastq.gz -o fastqc_out_entropy -t 30

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/fastqc_out_entropy/*html ./

# mapping with entropy filtered sequences of SRR10996962
screen -r 
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"
conda activate coverm-0.6.1

ls -1 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/*entropy*.fastq.gz | sed 's/.*_//' | sed 's/\..*//' | sort | uniq | while read line
do
bwa mem -v 1 -t 24 ../refgenome/ref_index_all_fungi.fna ${QT}/SRR10996962_1_${line}.fastq.gz ${QT}/SRR10996962_2_${line}.fastq.gz > SRR10996962_${line}.sam
samtools sort -T tmp_SRR10996962_${line}_samtools -@ 24 -O BAM -o SRR10996962_${line}.bam SRR10996962_${line}.sam
rm SRR10996962_${line}.sam
done

#coverm
conda activate coverm-0.6.1

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*entropy*.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 70 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_entropy_output.txt -t 30



# BBDuk entropy to remove  repetitive sequences in all metagenomes using entropy = 0.4
screen -r
conda deactivate
conda activate bbmap-39.01
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
bbduk.sh in=${line}_1_qt.fastq.gz in2=${line}_2_qt.fastq.gz out=${line}_1_entropy.fastq.gz out2=${line}_2_entropy.fastq.gz outm=${line}_1_discard.fastq.gz outm2=${line}_2_discard.fastq.gz entropy=0.4 entropywindow=30 entropyk=5 threads=30
done

# fastqc to check which effect the different entropy settings have 
conda deactivate
conda activate qc-env
fastqc *entropy.fastq.gz -o fastqc_out_entropy -t 30
fastqc *discard.fastq.gz -o fastqc_out_entropy -t 30

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/fastqc_out_entropy/*html ./

ls -1 *_1_entropy.fastq.gz | parallel -k -j30 "zcat {} | awk 'END {print NR/4}'" | paste <(ls -1 *_1_entropy.fastq.gz | sed 's/_1_entropy.fastq.gz//') - > nseq_entropy_out.txt

ls -1 *_1_qt.fastq.gz | parallel -k -j30 "zcat {} | awk 'END {print NR/4}'" | paste <(ls -1 *_1_qt.fastq.gz | sed 's/_1_qt.fastq.gz//') - > nseq_qt_out.txt

# mapping with entropy filtered sequences of all metagenomes
screen -r 
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
QT="/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming"
conda deactivate
conda activate coverm-0.6.1

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
bwa mem -v 1 -t 38 ../refgenome/ref_index_all_fungi.fna ${QT}/${line}_1_entropy.fastq.gz ${QT}/${line}_2_entropy.fastq.gz > ${line}_entropy.sam
samtools sort -T tmp_${line}_entropy_samtools -@ 38 -O BAM -o ${line}_entropy.bam ${line}_entropy.sam
rm ${line}_entropy.sam
done

#coverm
conda deactivate
conda activate coverm-0.6.1

coverm genome -b /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/*entropy.bam -f /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_all_chytrids/*.fna /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/fastaseq_refseq/*.fna --min-covered-fraction 0 --min-read-percent-identity 70 --min-read-aligned-percent 50 --proper-pairs-only --exclude-supplementary -o /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_entropy_output_all.txt -t 30

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/coverm/mapping_entropy_output_all.txt ./

#18S mapping with phyloflash
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash
conda activate phyloflash-3.4

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do 
phyloFlash.pl -lib ${line}_entropy -read1 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/${line}_1_entropy.fastq.gz -read2 /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/${line}_2_entropy.fastq.gz -dbhome /bio/Databases/SILVA/v138.1/phyloflash/138.1 -CPUs 48 -emirge > ${line}_entropy_pf.log 2>&1
done

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash/*html ./

#copy to local laptop
cd Documents/IOWseq000044_fungi_data_mining/reference_genome/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phyloflash/*_entropy.phyloFlash.NTUfull_abundance.csv ./


# run short read classification (kaiju) against all domains of life -> to find out what is the % fungi in the sample
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
ls -1 *_1_qt.fastq.gz | tr '\n' ','

screen -r 
R1=$(ls -1 *_1_entropy.fastq.gz | tr '\n' ',' | sed 's/,$//')
R2=$(ls -1 *_2_entropy.fastq.gz | tr '\n' ',' | sed 's/,$//')
OUT=$(ls -1 *_1_entropy.fastq.gz | xargs -n1 basename | sed 's/_1_entropy.fastq.gz//' | sed 's/$/_kaiju_entropy.txt/' | tr '\n' ',' | sed 's/,$//')
conda activate kaiju-1.9.2

kaiju-multi -z 38 -t /bio/Databases/Cross_domain/June2022/DB_taxonomy/nodes.dmp -f /bio/Databases/Cross_domain/June2022/kaiju/proteins.fmi -i ${R1} -j ${R2} -o ${OUT} -e 5 -E 0.1


cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming
conda activate taxonkit-0.14.0

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
cut -f3 ${line}_kaiju_entropy.txt | taxonkit lineage --data-dir /bio/Databases/Cross_domain/June2022/DB_taxonomy > ${line}_output_entropy.txt
done

# taxonkit summary
conda activate r-4.2.2

# R script requires list of input files in quotations:
IN=$(ls -1 *_output_entropy.txt | tr '\n' ' ')
/bio/Common_repositories/workflow_templates/metaG_Illumina_PE/scripts/summarize_kaiju.R -i "$IN" -o kaiju_entropy_summary.txt -t 90

# copy to local laptop
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/trimming/kaiju_entropy_summary.txt ./






# breadth of coverage automatical for all metagenomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
conda deactivate
conda activate msamtools-1.1.2

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j20 'msamtools filter -b -p 70 -z 50 {}_entropy.bam > {}_entropy_filt1.bam'


conda deactivate
conda activate coverm-0.6.1

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j30 'samtools view -f 2 -F 2304 -b {}_entropy_filt1.bam > {}_entropy_filt2.bam'

cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | parallel -j30 'samtools depth {}_entropy_filt2.bam > samtools_depth_entropy_{}.txt'
# -b -> write output in bam-format; -p -> threshold for %-identity; -z -> threshold for read-aligned-%

# summarize entries in samtools_depth_test.txt for every reference-genome
# 3 tables: 1. containing the samtools output -> how many positions/basepairs map the reference -> divided by regions of genome/chromosome; 2. containing the regions of genome/chromosome assigned to the reference-genome; 3. containing the genome size of every reference-genome
# 1st table
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping
cat /bio/Analysis_data/IOWseq000044_fungi_data_mining/Validated_data/run_accessions_* | while read line
do
cut -f1 samtools_depth_entropy_${line}.txt | sort | uniq -c | sed -e 's/^ *//' -e 's/ /\t/' > depth_samtools_output_entropy_${line}.txt
done

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/mapping/depth_samtools_output_entropy*.txt ./

### workflow using snakemake -> workflow_620_metagenomes and Snakefile
cd Documents/IOWseq000044_fungi_data_mining/reference_genome/depth_all_metagenomes/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/depth/*_depth.txt ./
cd Documents/IOWseq000044_fungi_data_mining/reference_genome
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/*_logs.txt ./
cd Documents/IOWseq000044_fungi_data_mining/fastqc_data/fastqc_checks/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/fastqc_checks/*html ./
cd Documents/IOWseq000044_fungi_data_mining/reference_genome/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/mapping*.txt ./
cd Documents/IOWseq000044_fungi_data_mining/reference_genome/pf_all_metagenomes/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/phyloflash/*/*.phyloFlash.NTUfull_abundance.csv ./

cd Documents/IOWseq000044_fungi_data_mining/reference_genome/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/mapping_output_add*.txt ./
# cd Documents/IOWseq000044_fungi_data_mining/reference_genome/pf_all_metagenomes/
# scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/phyloflash/*/*.phyloFlash.NTUfull_abundance.csv ./

mv *_depth.txt ./depth_all_metagenomes/

# phyloflash_analysis to get the whole taxonomic path of the chytrids
cd Documents/IOWseq000044_fungi_data_mining/reference_genome/pf_full_taxonomy/
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/phyloflash/*/*_full_taxonomy.txt ./

# Editing of the chl data and copy to local laptop
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/env_data
grep '^I' resu_modis/chl.txt | sed -r 's/^I \/ +//' | sed 's/://' | sed -r 's/ +/\t/g' | sed 's/\.\.\.\./NA/' | cut -f1,2,3,5 > chl_data_modis.txt
grep '^I' resu_viirs_npp/chl.txt | sed -r 's/^I \/ +//' | sed 's/://' | sed -r 's/ +/\t/g' | sed 's/\.\.\.\./NA/' | cut -f1,2,3,5 > chl_data_viirs_npp.txt
grep '^I' resu_viirs_nasa/chl.txt | sed -r 's/^I \/ +//' | sed 's/://' | sed -r 's/ +/\t/g' | sed 's/\.\.\.\./NA/' | cut -f1,2,3,5 > chl_data_viirs_nasa.txt
grep '^I' chl_all.txt | sed -r 's/^I \/ +//' | sed 's/://' | sed -r 's/ +/\t/g' | sed 's/\.\.\.\./NA/g' | cut -f1,2,3,5,6,7 > chl_data_all.txt

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/env_data/chl_data_modis.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/env_data/chl_data_viirs_npp.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/env_data/chl_data_viirs_nasa.txt ./
scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/env_data/chl_data_all.txt ./

### Phylogeny
scp bio-40:/bio/Databases/SILVA/v138.1/silva_138.1_nr99_acc2tax.txt ./

## PR2 see R 
dos2unix pr2_chytrids.fasta
scp ./pr2_chytrids.fasta bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/

## refgenome RNA Data Download
# server
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/
mkdir rna_data
# local laptop
dos2unix download_links_refgenome_rna.txt
scp ./download_links_refgenome_rna.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/rna_data

cat download_links_refgenome_rna.txt | parallel -j20 wget
gunzip GCA*

## Refgenomes
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/refgenome/rna_data
grep -i "rrna" GCA*
grep -i "18S rrna" GCA*

grep "gbkey=rRNA" GCA* | grep "18S" | cut -d' ' -f1 | sed 's/:/\t/' > tmp.accnos
conda activate bbmap-39.01
reformat.sh in=GCA_000149865.1_BD_JEL423_rna_from_genomic.fna out=GCA_000149865.1_BD_JEL423_rna_from_genomic_linear.fna fastawrap=1000000
grep "GCA_000149865.1_BD_JEL423_rna_from_genomic.fna" tmp.accnos | cut -f2 | grep -F -w -A1 -f - GCA_000149865.1_BD_JEL423_rna_from_genomic_linear.fna | sed '/^--$/d' > refgenomes_chytrids.fasta

reformat.sh in=GCA_000182565.2_S_punctatus_V1_rna_from_genomic.fna out=GCA_000182565.2_S_punctatus_V1_rna_from_genomic_linear.fna fastawrap=1000000
grep "GCA_000182565.2_S_punctatus_V1_rna_from_genomic.fna" tmp.accnos | cut -f2 | grep -F -w -A1 -f - GCA_000182565.2_S_punctatus_V1_rna_from_genomic_linear.fna | sed '/^--$/d' >> refgenomes_chytrids.fasta

## phyloflash
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/workflow_output/phyloflash/
grep -i "Chytridiomycota" *_pf_out/*.all.vsearch.csv

# concatenate all final fasta
cat ./*/*.all.final.fasta > pf_all_ssu.fasta
reformat.sh in=pf_all_ssu.fasta out=pf_all_ssu_linear.fasta fastawrap=1000000
grep -i "Chytridiomycota" *_pf_out/*.all.vsearch.csv | cut -f1 | cut -d':' -f2 | grep -F -w -A1 -f - pf_all_ssu_linear.fasta | sed '/^--$/d' > pf_chytrids.fasta

## silva
# reformat to linear (save in fungi dir)
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/silva/
reformat.sh in=/bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_tax_silva_trunc.fasta out=./SILVA_138.1_SSURef_NR99_tax_silva_trunc_linear.fasta fastawrap=1000000

dos2unix silva_accessions.txt
scp ./silva_accessions.txt bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/silva/

# export accession list from R
grep -F -w -A1 -f silva_accessions.txt SILVA_138.1_SSURef_NR99_tax_silva_trunc_linear.fasta | sed '/^--$/d' > silva_chytrids.fasta

## move all 4 fasta files to one certain directory
cd /bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/

cat *_chytrids.fasta > chytrids.fasta

## Alignment
conda activate qiime2-2021.2
sina -i chytrids.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o chytrids_aligned.fasta -t all -p 40 --overhang remove --insertion forbid --log-file chytrids_sina.log 

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/chytrids_aligned.fasta ./

## mothur -> don't work
module load
mothur "#summary.seqs(fasta=chytrids_aligned.fasta)"

## processing of the Alignment using seaview for removing common gaps and R for curating the Alignment
dos2unix chytrids_aligned_curated.fasta
scp ./chytrids_aligned_curated.fasta bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/

## fasttree
conda activate qiime2-2021.2
fasttree -gtr -gamma -nt chytrids_aligned_curated.fasta > ref.tree
# -gamma -> rescale branch length using Gamma20-based likelihood; -nt -> nucleotide alignment 

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/ref.tree ./

## adding sister groups of Chytridiomycota as outgroups for the phylogeny -> to get a rooted tree
dos2unix pr2_sister_groups_chytrids.fasta
scp ./pr2_sister_groups_chytrids.fasta bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/

cat *_chytrids.fasta > chytrids_rooted.fasta

## Alignment
conda activate qiime2-2021.2
sina -i chytrids_rooted.fasta -r /bio/Databases/SILVA/v138.1/SILVA_138.1_SSURef_NR99_12_06_20_opt.arb -o chytrids__rooted_aligned.fasta -t all -p 40 --overhang remove --insertion forbid --log-file chytrids_rooted_sina.log 

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/chytrids__rooted_aligned.fasta ./

## mothur -> don't work
# module load
# mothur "#summary.seqs(fasta=chytrids_aligned.fasta)"

## processing of the Alignment using seaview for removing common gaps and R for curating the Alignment
dos2unix chytrids__rooted_aligned_curated.fasta
scp ./chytrids__rooted_aligned_curated.fasta bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/

## fasttree
conda activate qiime2-2021.2
fasttree -gtr -gamma -nt chytrids__rooted_aligned_curated.fasta > rooted_ref.tree
# -gamma -> rescale branch length using Gamma20-based likelihood; -nt -> nucleotide alignment 

scp bio-40:/bio/Analysis_data/IOWseq000044_fungi_data_mining/Intermediate_results/phylogeny/rooted_ref.tree ./



